import Foundation
import NIO
import DiscordBM
import AsyncHTTPClient
import Vision
import AppKit

let wysiRegex = try! Regex("(7[^0-9]{0,3}2[^0-9]{0,3}7)")
var reactedTo: Array<OCRDetection> = []
var visualizedTo: Array<MessageSnowflake> = []

@main
public struct Bot {
    public static func main() async {
        let eventLoopGroup = MultiThreadedEventLoopGroup(numberOfThreads: 1)
        let httpClient = HTTPClient(eventLoopGroupProvider: .shared(eventLoopGroup))
        let bot = await BotGatewayManager(
            eventLoopGroup: eventLoopGroup,
            httpClient: httpClient,
            token: ProcessInfo.processInfo.environment["DISCORD_TOKEN"]!,
            intents: [.guildMessages, .guildMessageReactions]
        )
        
        await bot.connect()
        
        let clientUserId = try! await bot.client.getOwnUser().decode().id
        
        let stream = await bot.makeEventsStream()
        
        for await event in stream {
            EventHandler(event: event, client: bot.client, clientUserId: clientUserId).handle()
        }
    }
}

public struct OCRDetection {
    let replyTo: MessageSnowflake
    let rect: CGRect
    let cgImage: CGImage
}

public struct OCRHandler {
    let url: String
    let message: DiscordChannel.Message
    let client: any DiscordClient
    let cgImage: CGImage
    
    init(url: String, message: DiscordChannel.Message, client: any DiscordClient) {
        self.url = url
        self.message = message
        self.client = client
        
        let data = try? Data(contentsOf: URL(string: url)!)
        let image = NSImage(data: data!)!
        
        self.cgImage = image.cgImage(forProposedRect: nil, context: nil, hints: nil)!
    }
    
    func recognizeTextHandler(request: VNRequest, error: Error?) {
        guard let observations =
                request.results as? [VNRecognizedTextObservation] else {
            return
        }
        let recognizedStrings = observations.compactMap { observation in
            return observation.topCandidates(1).first
        }
        
        for part in recognizedStrings {
            /*if (part.confidence < 0.6) {
                continue
            }*/
            
            let ranges = part.string.ranges(of: wysiRegex)

            if (ranges.count == 0) {
                continue
            }
            
            if (reactedTo.filter{$0.replyTo == message.id}.count > 0) {
                continue
            }
                    
            let boxObservation = try? part.boundingBox(for: ranges.first!)
            let boundingBox = boxObservation?.boundingBox ?? .zero
            
            let rect = VNImageRectForNormalizedRect(boundingBox,
                                                    Int(self.cgImage.width),
                                                    Int(self.cgImage.height))
            
            Task {
                let ocrDetection = OCRDetection(replyTo: message.id, rect: rect, cgImage: self.cgImage)
                reactedTo.append(ocrDetection)
                
                do {
                    try await client.createMessage(
                        channelId: message.channel_id,
                        payload: .init(
                            content: "<a:WYSI:802721803755061301>",
                            message_reference: DiscordChannel.Message.MessageReference(message_id: message.id)
                        )
                    )
                } catch let e {
                    print(e)
                }
            }
            
            break
        }
    }
    
    func process() {
        let handler = VNImageRequestHandler(cgImage: self.cgImage)
        let request = VNRecognizeTextRequest(completionHandler: recognizeTextHandler)
        request.recognitionLevel = .fast
        
        do {
            try handler.perform([request])
        } catch {
            return
        }
    }
}

struct EventHandler: GatewayEventHandler {
    let event: Gateway.Event
    let client: any DiscordClient
    let clientUserId: UserSnowflake
    
    func checkEmbeds(embeds: Array<Embed>, message: DiscordChannel.Message) {
        var url: String = ""
        
        for embed in embeds {
            if (embed.image?.url != nil) {
                url = embed.image!.proxy_url!
            } else if (embed.url != nil) {
                url = embed.url!
            } else {
                return
            }
            
            OCRHandler(url: url, message: message, client: client).process()
        }
    }
    
    func onMessageUpdate(_ payload: DiscordChannel.PartialMessage) async {
        do {
            let message = try await client.getMessage(channelId: payload.channel_id, messageId: payload.id).decode()
            
            checkEmbeds(embeds: message.embeds, message: message)
        } catch let e {
            print(e)
        }
    }
        
    func onMessageCreate(_ payload: Gateway.MessageCreate) async {
        if (payload.author?.id == self.clientUserId) {
            return
        }
        
        do {
            let message = try await client.getMessage(channelId: payload.channel_id, messageId: payload.id).decode()
            
            for attachment in message.attachments {
                OCRHandler(url: attachment.url, message: message, client: client).process()
            }
            
            checkEmbeds(embeds: message.embeds, message: message)
        } catch let e {
            print(e)
        }
    }
    
    func onMessageReactionAdd(_ payload: Gateway.MessageReactionAdd) async {
        if (payload.emoji.name! != "❓" 
        && !payload.emoji.name!.lowercased().contains("question")) {
            return
        }
        
        if (payload.message_author_id != self.clientUserId) {
            return
        }
        
        if (visualizedTo.firstIndex(of: payload.message_id) != nil) {
            return
        }
        
        visualizedTo.append(payload.message_id)
        
        var messageId: MessageSnowflake
        
        do {
            let message = try await client.getMessage(
                channelId: payload.channel_id,
                messageId: payload.message_id
            ).decode()
            
            if (message.message_reference?.message_id == nil) {
                return
            }
            
            messageId = message.message_reference!.message_id!
        } catch let e {
            print(e)
            return
        }
        
        let ocrDetection = reactedTo.first(where: {$0.replyTo == messageId})
        
        if (ocrDetection == nil) {
            return;
        }
        
        let rect = ocrDetection!.rect
        let cgImage = ocrDetection!.cgImage
        
        let width = cgImage.width
        let height = cgImage.height
        
        let cgContext = CGContext(
            data: nil,
            width: width,
            height: height,
            bitsPerComponent: cgImage.bitsPerComponent,
            bytesPerRow: 0,
            space: CGColorSpaceCreateDeviceRGB(),
            bitmapInfo: CGImageAlphaInfo.noneSkipLast.rawValue
        )!
        
        cgContext.draw(cgImage, in: CGRect(x: 0, y: 0, width: width, height: height))
        cgContext.setStrokeColor(red: 1.0, green: 0, blue: 0, alpha: 1.0)
        cgContext.setLineWidth(2.5)
        cgContext.stroke(rect)
        
        let ciContext = CIContext()
        let ciImage = CIImage(cgImage: cgContext.makeImage()!)
        
        Task {
            do {
                var imageData = ciContext.pngRepresentation(of: ciImage, format: .RGBA8, colorSpace: ciImage.colorSpace!)
                
                if (imageData!.count > 1024 * 1024) {
                    imageData = ciContext.jpegRepresentation(of: ciImage, colorSpace: ciImage.colorSpace!)
                }
                   
                try await client.createMessage(
                    channelId: payload.channel_id,
                    payload: .init(
                        message_reference: DiscordChannel.Message.MessageReference(message_id: payload.message_id),
                        files: [RawFile(data: ByteBuffer(data: imageData!), filename: "where.jpg")]
                    )
                )
            } catch let e {
                print(e)
            }
        }
    }
}
